import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    usersDB: {
      data: {
        '1': {
          avatar: 'http://localhost:8080/images/avatars/AnnetStark.png',
          name: 'Annet Stark',
          password: '@annetstark',
          email: 'annet.s@filancy.com',
          chatRoomsIds: ['1', '2', '5'],
        },
        '2': {
          avatar: 'http://localhost:8080/images/avatars/1112.png',
          name: 'Jhon Doe',
          password: '@johndoe',
          email: 'john.d@filancy.com',
          chatRoomsIds: ['5'],
        },
        '3': {
          avatar: 'http://localhost:8080/images/avatars/1111.png',
          name: 'Merry Wix',
          password: '@merrywix',
          email: 'merry.w@filancy.com',
          chatRoomsIds: ['1'],
        },
        '4': {
          avatar: 'http://localhost:8080/images/avatars/1113.png',
          name: 'Mike Berman',
          password: '@mikeberman',
          email: 'mike.b@filancy.com',
          chatRoomsIds: ['2'],
        },
      },
      ids: ['1', '2', '3', '4'],
    },
    chatsDB: {
      data: {
        '1': {
          ids: ['1', '3'],
          dialog: [
            { id: '1', userId: '1', date: '23.02.2021', message: 'Hi!' },
            { id: '2', userId: '3', date: '23.02.2021', message: 'Whats up!' },
            {
              id: '3',
              userId: '1',
              date: '23.02.2021',
              message: `Let's go to the park!`,
            },
            {
              id: '4',
              userId: '3',
              date: '23.02.2021',
              message: `Ok. I'm on my way.`,
            },
          ],
        },
        '2': {
          ids: ['1', '4'],
          dialog: [
            {
              id: '5',
              userId: '1',
              date: '23.02.2021',
              message: 'I say BeBeBe!',
            },
            {
              id: '6',
              userId: '4',
              date: '23.02.2021',
              message: 'So get from me MeMeMe',
            },
            {
              id: '7',
              userId: '1',
              date: '23.02.2021',
              message: `We have very philosofical conversation`,
            },
            {
              id: '8',
              userId: '4',
              date: '23.02.2021',
              message: `Yes, it means we are exist`,
            },
          ],
        },
        '5': {
          ids: ['1', '2'],
          dialog: [
            {
              id: '5',
              userId: '1',
              date: '23.02.2021',
              message: 'I say BeBeBe!',
            },
            {
              id: '6',
              userId: '2',
              date: '23.02.2021',
              message: 'So get from me MeMeMe',
            },
            {
              id: '7',
              userId: '1',
              date: '23.02.2021',
              message: `We have very philosofical conversation`,
            },
            {
              id: '8',
              userId: '2',
              date: '23.02.2021',
              message: `Konichiua`,
            },
            {
              id: '9',
              userId: '2',
              date: '23.02.2021',
              message: `Uno`,
            },
          ],
        },
      },
      ids: ['1', '2', '5'],
    },
  },
  mutations: {
    SET_NEW_MESSAGE(state, { chatId, ...rest }) {
      state.chatsDB.data[chatId].dialog.push({
        id: rest.id,
        userId: rest.userId,
        message: rest.message,
      })
    },
  },
  actions: {},
  getters: {
    getChatterList: state => sessionUserId => {
      const chatRoomsIds = state.usersDB.data[sessionUserId].chatRoomsIds
      const chattersList = chatRoomsIds.map(id => {
        const chatRoom = state.chatsDB.data[id]
        const interlocutorId = chatRoom.ids.find(i => i != sessionUserId)
        const { avatar, name } = state.usersDB.data[interlocutorId]
        const { message, date } = chatRoom.dialog[chatRoom.dialog.length - 1]

        return {
          id, //<- chat room id
          avatar,
          name,
          text: message,
          date,
          chatId: 'none',
        }
      })
      return chattersList
    },
    getChatDialogList: state => chatId => {
      return state.chatsDB.data[chatId].dialog
    },
    // getLastMessage: state => sessionUserId => {},
  },
  modules: {},
})
